-- collide all physical objects
-- phy obj tiles have flag 0
function collide_phy(obj,aim,flag)
	--obj = table needs x,y,w,h
	--aim = left,right,up,down

	local x=obj.x  local y=obj.y
	local w=obj.w  local h=obj.h

	local x1=0	 local y1=0
	local x2=0  local y2=0

	if aim=="left" then
		x1=x-1		y1=y
		x2=x		y2=y+h-1
	elseif aim=="right" then
		x1=x+w-1	y1=y
		x2=x+w		y2=y+h-1
	elseif aim=="up" then
		x1=x+2		y1=y-1
		x2=x+w-3	y2=y
	elseif aim=="down" then
		x1=x+2		y1=y+h
		x2=x+w-3	y2=y+h
	end

	--pixels to tiles
	x1/=8    y1/=8
	x2/=8    y2/=8

	if fget(mget(x1,y1), flag)
	or fget(mget(x1,y2), flag)
	or fget(mget(x2,y1), flag)
	or fget(mget(x2,y2), flag) then
		return true
	else
		return false
	end
end

function abs_box(s)
	local box = {}
	box.x1 = s.box.x1 + s.x
	box.y1 = s.box.y1 + s.y
	box.x2 = s.box.x2 + s.x
	box.y2 = s.box.y2 + s.y
	return box
end

function coll(a,b)
	local box_a = abs_box(a)
	local box_b = abs_box(b)

	if box_a.x1 > box_b.x2 or
	box_a.y1 > box_b.y2 or
	box_b.x1 > box_a.x2 or
	box_b.y1 > box_a.y2 then
		return false
 	end
	return true 
end