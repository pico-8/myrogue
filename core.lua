function actors_update()
    player_actions()
    for a in all(actors) do
        if a.pv<=0 then del(actors,a) end
        actors_moves(a)
        actors_bumped(a)
        actors_attacks(a)
        check_collisions_actors_n_map(a)
        if #aboxes==0 then
            a.touched=false -- if no attack box, no one touched
            if a.type=="player" then a.sp=17 else a.sp=4 end -- ugly touched effect
        end
    end
end

function player_update()
    --check_ennemy_collision()
end

function check_collisions_att_boxes_n_actors()
    for b in all(aboxes) do
        --check_collisions_attacks_n_actors(b)
        for a in all(actors) do
            if coll(a,b) then
                if not b.owner!=a -- check not the owner of attack is himself
                and b.owner.type!=a.type -- no friendly fire
                and a.touched==false then -- first box & actor contact
                    a.touched=true
                    a.sp=20
                    deal_damage(b.owner,a)
                    bump(a,b)
                end
            else
                a.touched=false
                if a.type=="player" then a.sp=17 else a.sp=4 end -- ugly touched effect
            end
        end
    end
end

function player_actions()
    if btn(⬅️) then p.inptl=true else p.inptl=false end
	if btn(➡️) then p.inptr=true else p.inptr=false end
	if btn(⬆️) then p.inptu=true else p.inptu=false end
    if btn(⬇️) then p.inptd=true else p.inptd=false end
    if btn(❎) then p.attacking=true else p.attacking=false end
end

function actors_moves(a)
    --co: coeff movement

    --diags
    if a.inptr and a.inptu then a.dx=1 a.dy=-1 set_last_dir(a,{1,-1})
    elseif a.inptr and a.inptd then a.dx=1 a.dy=1 set_last_dir(a,{1,1})
    elseif a.inptl and a.inptu then a.dx=-1 a.dy=-1 set_last_dir(a,{-1,-1})
    elseif a.inptl and a.inptd then a.dx=-1 a.dy=1 set_last_dir(a,{-1,1})
    --normals
    elseif a.inptl then a.dx=-1 a.dy=0 set_last_dir(a,{-1,0})
    elseif a.inptr then a.dx=1 a.dy=0 set_last_dir(a,{1,0})
    elseif a.inptu then a.dx=0 a.dy=-1 set_last_dir(a,{0,-1})
    elseif a.inptd then a.dx=0 a.dy=1 set_last_dir(a,{0,1})

    elseif a.type=="ennemy" and not a.bumped then
        a.pangle=atan2(p.x-a.x,p.y-a.y)
        a.dx=.3*cos(a.pangle)
        a.dy=.3*sin(a.pangle)
    
    elseif not a.bumped and a.type=="player" then a.dx=0 a.dy=0 end

    a.x+=a.dx
    a.y+=a.dy
end

function set_last_dir(a,dir)
    a.ldir=dir
    if is_in(0,a.ldir) then
        a.lrdir={a.dx,a.dy}
    end
end

function actors_bumped(a)
    if a.bumped_timer>0 then
        a.bumped=true
        a.dx=a.bdir[1]*bump_coeff
        a.dy=a.bdir[2]*bump_coeff
        a.bumped_timer-=1
    else
        a.bumped=false
    end
end

function actors_attacks(a)
    if a.attacking and not a.input_attack then
        a.input_attack_pressed=4
        manage_attack_box(a)
    else
        a.input_attack_pressed=a.attacking and max(0,a.input_attack_pressed-1) or 0
    end
    a.input_attack=a.attacking
end

function check_collisions_actors_n_map(a)
    --check collision up and down
    if a.dy>0 then
        if collide_phy(a,"down",0) then
            a.dy=0
            a.y-=((a.y+a.h+1)%8)-1
        end
    elseif a.dy<0 then
        if collide_phy(a,"up",0) then
            a.dy=0
            a.y-=((a.y+a.h+1)%8)-1
        end
    end
    
    --check collision left and right
    if a.dx<0 then
        if collide_phy(a,"left",0) then
            a.dx=0
            a.x-=((a.x+a.w+1)%8)-1
        end
    elseif a.dx>0 then
        if collide_phy(a,"right",0) then
            a.dx=0
            a.x-=((a.x+a.w+1)%8)-1
        end
    end
end

function check_collisions_attacks_n_actors(b)
    --
end

function manage_attack_box(a,b)
    -- create or update attack box
    local b=b or init_attack_box(a)

    if b.timer>0 then
        local x=a.x             local w=a.w
        local y=a.y             local h=a.h
        local dx=a.lrdir[1]      local dy=a.lrdir[2]

        if dx==0 and dy==-1 then
            b.x=x
            b.y=y-h
        elseif dx==1 and dy==0 then
            b.x=x+w
            b.y=y
        elseif dx==0 and dy==1 then
            b.x=x
            b.y=y+h
        elseif dx==-1 and dy==0 then
            b.x=x-w
            b.y=y
        end
        b.dir={dx,dy}
    else
        del(aboxes,b)
        do return end
    end

    add(aboxes,b)
end

function get_attack_box_origin_point(dir)
    local dx=dir[1]
    local dy=dir[2]
    local result={x=0,y=0}

    if dx==0 and dy==-1 then
        result.x=x
        result.y=y-h
    elseif dx==1 and dy==0 then
        result.x=x+w
        result.y=y
    elseif dx==0 and dy==1 then
        result.x=x
        result.y=y+h
    elseif dx==-1 and dy==0 then
        result.x=x-w
        result.y=y
    end

    return result
end

function update_attack_box(b)
    if b.timer == 0 then
        del(aboxes,b)
    else
        local x=b.owner.x             local w=b.owner.w
        local y=b.owner.y             local h=b.owner.h
        local dx=b.owner.lrdir[1]     local dy=b.owner.lrdir[2]

        if dx==0 and dy==-1 then
            b.x=x
            b.y=y-h
        elseif dx==1 and dy==0 then
            b.x=x+w
            b.y=y
        elseif dx==0 and dy==1 then
            b.x=x
            b.y=y+h
        elseif dx==-1 and dy==0 then
            b.x=x-w
            b.y=y
        end

        b.timer-=1
    end
end

function check_ennemy_collision()
    for a in all(actors) do
        if a.type=="ennemy" then
            if coll(p,a) then
                --deal_damage(a,p)
            end
        end
    end
end

-- s: source
-- v: victim
function deal_damage(s,v)
    v.pv-=s.attack.dmg
end

function att_boxes_update()
    for b in all(aboxes) do
        update_attack_box(b)
        -- manage_attack_box(b.owner,b)
    end
end

function bump(a,b)
    a.bumped_timer=bump_timer
    local angle=atan2(a.x-b.owner.x,a.y-b.owner.y)
    a.bdir[1]=1*cos(angle)
    a.bdir[2]=1*sin(angle)
end

function debug(deb)
    local ctn=0
    for d in all(deb) do
        print(d[1]..": "..d[2],2,120-ctn*6,15)
        ctn+=1
    end
end